#!/usr/bin/env python3

from gpiozero import TrafficLights, Button, PWMOutputDevice
from time import sleep


def main():
    lights = TrafficLights(14, 15, 18)
    lights.off()
    lights.green.on()
    pedestrian_button = Button(23)
    buzzer = PWMOutputDevice(12)
    buzzer.frequency = 444
    buzzer.value = 0.5
    buzzer.off()
    while True:
        pedestrian_button.wait_for_press()
        pedestrian_call(lights, buzzer)


def pedestrian_call(lights, buzzer):
    buzz(buzzer, 1)
    sleep(3)
    lights.green.off()
    lights.amber.on()
    sleep(4)
    lights.amber.off()
    lights.red.on()
    buzz(buzzer, 2)
    sleep(5)
    buzz(buzzer, 3)
    sleep(3)
    lights.red.off()
    lights.green.on()


def buzz(buzzer, n=1):
    for i in range(n):
        buzzer.value = 0.5
        sleep(0.1)
        buzzer.off()
        sleep(0.1)

if __name__ == "__main__":
    main()
